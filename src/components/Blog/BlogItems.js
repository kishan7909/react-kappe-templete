/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import BlogSlider from "./BlogSlider";

const BlogItems = props => {
    return (
        <div className="blog-post gallery-post">
            <div className="inner-post">
                <div
                    id={"myCarousel" + props.id}
                    className="carousel slide"
                    data-ride="carousel"
                >
                    <div className="carousel-inner">
                        {props.blog.image.map((img, index) => (
                            <BlogSlider
                                key={index}
                                image={img}
                                active={index}
                            />
                        ))}
                    </div>
                    <a
                        className="left carousel-control"
                        href={"#myCarousel" + props.id}
                        data-slide="prev"
                    >
                        <span className="sr-only">Previous</span>
                    </a>
                    <a
                        className="right carousel-control"
                        href={"#myCarousel" + props.id}
                        data-slide="next"
                    >
                        <span className="sr-only">Next</span>
                    </a>
                </div>
                <div className="post-content">
                    <h2>
                        <a href="single-post.html">{props.blog.title}</a>
                    </h2>
                    <p>{props.blog.text}</p>
                </div>
                <ul className="post-tags">
                    <li>
                        <a href="#">
                            <i className="fa fa-calendar" />
                            {props.blog.date}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default BlogItems;
