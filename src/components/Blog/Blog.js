/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import BlogItems from "./BlogItems";
import Sidebar from "../Navigation/Sidebar";
import { connect } from "react-redux";

const Blog = props => {
    // const bloglength = props.blog.length;

    return (
        <React.Fragment>
            <Sidebar filter={false} />
            <div id="content">
                <div className="inner-content">
                    <div className="blog-page">
                        <div className="blog-box">
                            {props.blog.map(blog => (
                                <BlogItems
                                    key={blog.id}
                                    id={blog.id}
                                    blog={blog}
                                />
                            ))}
                        </div>
                    </div>
                    <div>
                        <span className="blog-page-link">
                            <span className="glyphicon glyphicon-chevron-left" />
                        </span>
                        <span className="blog-page-link">1</span>
                        <span className="blog-page-link">2</span>
                        <span className="blog-page-link">3</span>
                        <span className="blog-page-link">
                            <span className="glyphicon glyphicon-chevron-right" />
                        </span>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};
const mapStateToProps = state => {
    return {
        blog: state.blog.blog
    };
};

export default connect(mapStateToProps)(Blog);
