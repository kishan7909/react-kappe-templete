import React from "react";

const blogSlider = props => {
    return (
        <div className={props.active === 0 ? "item active" : "item"}>
            <img
                src={props.image}
                alt="Los Angeles"
                style={{ width: "100%", height: "250px" }}
            />
        </div>
    );
};

export default blogSlider;
