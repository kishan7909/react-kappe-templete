import React, { Component } from "react";
import { NavLink as Link } from "react-router-dom";

class SidebarLinks extends Component {
    state = {};
    render() {
        return (
            <div className="menu-box">
                <ul className="menu">
                    <li>
                        <Link exact to="/">
                            <span>Home</span>
                        </Link>
                    </li>
                    <li>
                        <Link exact to="/about">
                            <span>About</span>
                        </Link>
                    </li>
                    <li>
                        <Link exact to="/blog">
                            <span>blog</span>
                        </Link>
                    </li>
                    <li>
                        <Link exact to="/contact">
                            <span>Contact</span>
                        </Link>
                    </li>
                </ul>
            </div>
        );
    }
}

export default SidebarLinks;
