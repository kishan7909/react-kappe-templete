/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { NavLink as Link } from "react-router-dom";
import logo from "../../assert/images/logo.png";
import SidebarLinks from "./SidebarLinks";
import SidebarFilter from "./SidebarFilter";
import SidebarSocialLinks from "./SidebarSocialLinks";

class Sidebar extends Component {
    state = {};
    render() {
        return (
            //Header--------------------------------------
            <header style={{ height: "4526px" }}>
                {/* Header Logo-------------------- */}
                <div className="logo-box">
                    <Link to="/" className="logo">
                        <img alt="" src={logo} />
                    </Link>
                </div>
                {/* Header Menu-------------------- */}
                <Link className="elemadded responsive-link" to="#">
                    Menu
                </Link>

                <SidebarLinks project={this.props.project} />

                {/* Project filters-------------------------------------- */}
                {this.props.filter === true ? <SidebarFilter /> : null}

                {/* Header Social Box------------------------- */}
                <SidebarSocialLinks />
            </header>
        );
    }
}

export default Sidebar;
