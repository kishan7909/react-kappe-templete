import React, { Component } from "react";
import { NavLink as Link } from "react-router-dom";

class SidebarSocialLinks extends Component {
    state = {};
    render() {
        return (
            <div className="social-box">
                <ul className="social-icons">
                    <li>
                        <Link to="#" className="facebook">
                            <i className="fa fa-facebook" />
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="twitter">
                            <i className="fa fa-twitter" />
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="google">
                            <i className="fa fa-google-plus" />
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="linkedin">
                            <i className="fa fa-linkedin" />
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="pinterest">
                            <i className="fa fa-pinterest" />
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="youtube">
                            <i className="fa fa-youtube" />
                        </Link>
                    </li>
                    <li>
                        <Link to="#" className="github">
                            <i className="fa fa-github" />
                        </Link>
                    </li>
                </ul>
                <p className="copyright">
                    &#169; 2014 Kappe, All Rights Reserved
                </p>
            </div>
        );
    }
}

export default SidebarSocialLinks;
