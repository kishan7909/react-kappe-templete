import React, { Component } from "react";
import Sidebar from "../Navigation/Sidebar";
import ContactForm from "./ContactForm";

class Contact extends Component {
    state = {};
    render() {
        return (
            <React.Fragment>
                <Sidebar filter={false} />
                <div id="content">
                    <div className="inner-content">
                        <div className="contact-page">
                            <div id="map" />
                            <ContactForm />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Contact;
