/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { connect } from "react-redux";

const Infobox = props => {
    console.log(props);
    return (
        <div className="info-box">
            <a className="info-toggle" href="#">
                <i className="fa fa-info-circle" />
            </a>
            <div className="info-content">
                <ul>
                    <li>
                        <i className="fa fa-phone" />
                        {props.infobox.contactnumber}
                    </li>
                    <li>
                        <i className="fa fa-envelope" />
                        <a href="#">{props.infobox.mail}</a>
                    </li>
                    <li>
                        <i className="fa fa-home" />
                        {props.infobox.address}
                    </li>
                </ul>
            </div>
        </div>
    );
};
const mapStateToProps = state => {
    return {
        infobox: state.infobox
    };
};

export default connect(mapStateToProps)(Infobox);
