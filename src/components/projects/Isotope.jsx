import ImagesLoaded from "imagesloaded";
import Isotope from "isotope-layout";

export const IsotopeInit = isotopeContainer => {
    const container = document.getElementsByClassName(isotopeContainer)[0];
    ImagesLoaded(container, () => {
        new Isotope(container, {
            filter: "*",
            animationOptions: {
                duration: 750,
                easing: "linear"
            }
        });
    });
};

export const IsotopFilter = (isotopeContainer, selector) => {
    const container = document.getElementsByClassName(isotopeContainer)[0];

    Array.prototype.forEach.call(container.children, child => {
        child.removeAttribute("style");
    });

    new Isotope(container).arrange({
        filter: selector,
        layoutMode: "fitRows"
    });
};
