import React, { Component } from "react";

import { Link } from "react-router-dom";

class ProjectList extends Component {
    state = {};
    render() {
        return (
            <div className={"project-post " + this.props.project.type}>
                <img alt="" src={this.props.project.image} />
                <div className="hover-box">
                    <div className="project-title">
                        <h2>{this.props.project.title}</h2>
                        <span>{this.props.project.title2}</span>
                        <div>
                            <Link to={"project/" + this.props.project.id}>
                                <i className="fa fa-arrow-right" />
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProjectList;
