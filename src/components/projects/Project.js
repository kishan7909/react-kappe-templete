import React, { Component } from "react";
import ProjectList from "./ProjectList";
import { connect } from "react-redux";
import Sidebar from "../Navigation/Sidebar";
import { IsotopeInit } from "./Isotope";

class Project extends Component {
    state = {};
    componentDidMount() {}
    render() {
        return (
            <React.Fragment>
                <Sidebar filter={true} project={true} />
                <div id="content">
                    <div className="inner-content">
                        <div className="portfolio-page">
                            <div
                                className="portfolio-box"
                                onLoad={() => IsotopeInit("portfolio-box")}
                            >
                                {this.props.project.map(project => {
                                    return (
                                        <ProjectList
                                            key={project.id}
                                            project={project}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        project: state.projects.project
    };
};

export default connect(mapStateToProps)(Project);
