import React, { Component } from "react";

class ProjectFeature extends Component {
    state = {};
    render() {
        return (
            <div className="project-feature">
                <h1>Project Feature</h1>
                <ul>
                    {this.props.projectFeature.map((project, index) => {
                        return <li key={index}>{project}</li>;
                    })}
                </ul>
            </div>
        );
    }
}

export default ProjectFeature;
