/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";

class ProjectInfo extends Component {
    state = {};
    render() {
        return (
            <div className="post-info">
                <h1>Project Info</h1>
                <ul>
                    <li>
                        <span>
                            <i className="fa fa-user" />
                        </span>
                        <a href="#">{this.props.projectinfo.projectName}</a>
                    </li>
                    <li>
                        <span>
                            <i className="fa fa-heart" />
                        </span>
                        <a href="#">{this.props.projectinfo.likes} Likes</a>
                    </li>
                    <li>
                        <span>
                            <i className="fa fa-calendar" />
                        </span>
                        <a href="#">{this.props.projectinfo.dates}</a>
                    </li>
                    <li>
                        <span>
                            <i className="fa fa-link" />
                        </span>
                        <a href="#">{this.props.projectinfo.website}</a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default ProjectInfo;
