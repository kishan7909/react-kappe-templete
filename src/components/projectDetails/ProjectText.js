import React, { Component } from "react";

class ProjectText extends Component {
    state = {};
    render() {
        return (
            <div className="project-text" style={{ marginTop: "10px" }}>
                <h1 style={{ fontWeight: "80" }}>{this.props.title}</h1>
                <p>{this.props.text}</p>
            </div>
        );
    }
}

export default ProjectText;
