/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";

class ProjectGallery extends Component {
    state = {};
    render() {
        return (
            <div className="project-gallery">
                <h1>Project Gallery</h1>
                <ul>
                    {this.props.projectGallery.map((img, index) => {
                        return (
                            <li key={index}>
                                <a href="#">
                                    <img alt="" src={img} />
                                </a>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

export default ProjectGallery;
