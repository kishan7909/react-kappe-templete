import React, { Component } from "react";
import img1 from "../../assert/upload/photo1.jpg";
import img2 from "../../assert/upload/photo2.jpg";

class ProjectSlider extends Component {
    state = {
        img1: img1,
        img2: img2
    };
    render() {
        return (
            <div
                id="myCarousel"
                className="carousel slide"
                data-ride="carousel"
            >
                <ol className="carousel-indicators">
                    <li
                        data-target="#myCarousel"
                        data-slide-to="0"
                        className="active"
                    />
                    <li data-target="#myCarousel" data-slide-to="1" />
                    <li data-target="#myCarousel" data-slide-to="2" />
                </ol>

                <div className="carousel-inner">
                    <div className="item active">
                        <img
                            src={img1}
                            alt="Los Angeles"
                            style={{ width: "100%" }}
                        />
                    </div>

                    <div className="item">
                        <img
                            src={img2}
                            alt="Chicago"
                            style={{ width: "100%" }}
                        />
                    </div>

                    <div className="item">
                        <img
                            src={img1}
                            alt="New york"
                            style={{ width: "100%" }}
                        />
                    </div>
                </div>

                <a
                    className="left carousel-control"
                    href="#myCarousel"
                    data-slide="prev"
                >
                    <span className="sr-only">Previous</span>
                </a>
                <a
                    className="right carousel-control"
                    href="#myCarousel"
                    data-slide="next"
                >
                    <span className="sr-only">Next</span>
                </a>
            </div>
        );
    }
}

export default ProjectSlider;
