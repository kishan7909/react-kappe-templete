import React from "react";

const AboutSkillProgress = props => {
    return (
        <div className="skills-progress">
            <h1>Our Skills</h1>
            {props.ourSkills.map((skill, index) => {
                return (
                    <React.Fragment key={index}>
                        <p>
                            {skill.skill} <span>{skill.skill_Percentage}</span>
                        </p>
                        <div className="meter nostrips frontend">
                            <span style={{ width: skill.skill_Percentage }} />
                        </div>
                    </React.Fragment>
                );
            })}
        </div>
    );
};

export default AboutSkillProgress;
