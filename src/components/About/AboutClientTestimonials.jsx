import React from "react";

const AboutClientTestimonials = props => {
    return (
        <div className="testimonial">
            <h1>Client Testimonials</h1>
            <div className="quovolve-box play">
                <ul
                    className="quovolve"
                    style={{ position: "relative", height: "auto" }}
                >
                    {props.clients.map((client, index) => {
                        return (
                            <li key={index}>
                                <div className="client-test">
                                    <img alt="" src={client.image} />
                                    <h3>
                                        {client.name} - {client.skill}
                                    </h3>
                                </div>
                                <p>{client.blog}</p>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
};

export default AboutClientTestimonials;
