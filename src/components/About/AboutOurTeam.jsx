import React from "react";

const AboutOurTeam = props => {
    return (
        <div className="about-section last-section">
            <h1>Meet The Team</h1>
            <div className="team-members">
                <div className="row">
                    {props.ourTeams.map((team, index) => {
                        return (
                            <div key={index} className="col-md-4">
                                <div className="team-post">
                                    <img alt="" src={team.image} />
                                    <div className="team-hover">
                                        <div className="team-data">
                                            <h3>{team.name}</h3>
                                            <span>{team.skill}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};

export default AboutOurTeam;
