import React from "react";

const AboutUs = props => {
    return (
        <div className="about-section">
            <img alt="" src="./../../assert/upload/about.jpg" />
            <h1>About us</h1>
            {props.content.map((content, index) => (
                <p key={index}>{content}</p>
            ))}
        </div>
    );
};

export default AboutUs;
