import React, { Component } from "react";
import AboutUs from "./AboutUs";
import AboutOurTeam from "./AboutOurTeam";
import AboutSkillProgress from "./AboutSkillProgress";
import AboutClientTestimonials from "./AboutClientTestimonials";
import Sidebar from "../Navigation/Sidebar";

import { connect } from "react-redux";

class About extends Component {
    state = {};

    render() {
        console.log("aboutus", this.props.about);
        const content = this.props.about.content;
        const ourTeams = this.props.about.ourTeams;
        const ourSkills = this.props.about.ourSkills;
        const clients = this.props.about.clientTestimonials;
        return (
            <React.Fragment>
                <Sidebar filter={false} project={false} />
                <div id="content">
                    <div className="inner-content">
                        <div className="about-page">
                            <div className="about-box">
                                <div className="about-content">
                                    <AboutUs content={content} />
                                    <AboutOurTeam ourTeams={ourTeams} />
                                </div>
                                <div className="sidebar">
                                    <AboutSkillProgress ourSkills={ourSkills} />
                                    <AboutClientTestimonials
                                        clients={clients}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        about: state.about.about
    };
};

export default connect(mapStateToProps)(About);
