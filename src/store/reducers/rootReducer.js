import sidebarReducer from "./sidebarReducer";
import projectReducer from "./projectReducer";
import { combineReducers } from "redux";
import blogReducer from "./blogReducer";
import aboutReducer from "./aboutReducer";
import infoBoxReducer from "./infoBoxReducer";

console.log("rootReducer call");
const rootReducer = combineReducers({
    sidebar: sidebarReducer,
    projects: projectReducer,
    blog: blogReducer,
    about: aboutReducer,
    infobox: infoBoxReducer
});

export default rootReducer;
