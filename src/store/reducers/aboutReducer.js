import img1 from "../../assert/upload/team1.jpg";
import img2 from "../../assert/upload/team2.jpg";
import img3 from "../../assert/upload/team3.jpg";
import img4 from "../../assert/upload/team5.jpg";
import img5 from "../../assert/upload/team4.jpg";
import img6 from "../../assert/upload/team1.jpg";
import clientimg1 from "../../assert/upload/avatar1.jpg";
import clientimg2 from "../../assert/upload/avatar2.jpg";
import clientimg3 from "../../assert/upload/avatar3.jpg";
const initstate = {
    about: {
        content: [
            "This is Photoshops version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.",
            "Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim."
        ],
        ourTeams: [
            {
                id: 1,
                name: "Kishan Patel",
                skill: "Web Developer",
                image: img1
            },
            {
                id: 2,
                name: "Yash Rana",
                skill: "Mobile Developer",
                image: img2
            },
            {
                id: 3,
                name: "Jenil Topiwala",
                skill: "Java Developer",
                image: img3
            },
            {
                id: 4,
                name: "Yash Patel",
                skill: "Photoshops",
                image: img4
            },
            {
                id: 5,
                name: "Maulesh Jariwala",
                skill: "PHP Developer",
                image: img5
            },
            {
                id: 6,
                name: "Jaimin",
                skill: ".NET Developer",
                image: img6
            },
            {
                id: 7,
                name: "Sagar Panchal",
                skill: "Frontend Developer",
                image: img3
            }
        ],
        ourSkills: [
            {
                id: 1,
                skill: "Frontend Development",
                skill_Percentage: "71%"
            },
            {
                id: 2,
                skill: "Photoshop",
                skill_Percentage: "75%"
            },
            {
                id: 3,
                skill: "Wordpress",
                skill_Percentage: "80%"
            },
            {
                id: 4,
                skill: "Plugins",
                skill_Percentage: "91%"
            },
            {
                id: 5,
                skill: "Java",
                skill_Percentage: "87%"
            }
        ],
        clientTestimonials: [
            {
                name: "John Smith",
                skill: "Web Developer",
                blog:
                    "Sollicitudin lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt.",
                image: clientimg1
            },
            {
                name: "Collis Ta’eed ",
                skill: "CEO at Envato",
                blog:
                    "Sollicitudin lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt.",
                image: clientimg2
            },
            {
                name: "Kishan Patel",
                skill: "CEO at S.R Infotech",
                blog:
                    "Sollicitudin lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt.",
                image: clientimg3
            }
        ]
    }
};

const aboutReducer = (state = initstate, action) => {
    console.log("BlogReducer call", action);
    return state;
};

export default aboutReducer;
