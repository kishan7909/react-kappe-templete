const initstate = {
  sidebarlinks: [
    {
      id: 1,
      title: "Home",
      to: "/"
    },
    {
      id: 2,
      title: "About",
      to: "/about"
    },
    {
      id: 3,
      title: "Blog",
      to: "/blog"
    },
    {
      id: 4,
      title: "Contact",
      to: "/contact"
    }
  ]
};

const sidebarReducer = (state = initstate, action) => {
  console.log("sidebarReducer call", action);
  return state;
};

export default sidebarReducer;
