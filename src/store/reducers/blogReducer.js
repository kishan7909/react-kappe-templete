import img1 from "../../assert/upload/1.jpg";
import img2 from "../../assert/upload/2.jpg";
import img3 from "../../assert/upload/3.jpg";
import img4 from "../../assert/upload/4.jpg";
const initstate = {
    blog: [
        {
            id: 1,
            title: "Gallery Post Example",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img1, img2, img3],
            date: "Dec 19, 2013"
        },
        {
            id: 2,
            title: "Gallery Post",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img4],
            date: "Dec 21, 2013"
        },
        {
            id: 3,
            title: "Gallery Post",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img4],
            date: "Dec 21, 2013"
        },
        {
            id: 4,
            title: "Gallery Post Example",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img1, img2, img3],
            date: "Dec 19, 2013"
        },
        {
            id: 5,
            title: "Gallery Post",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img4],
            date: "Dec 21, 2013"
        },
        {
            id: 6,
            title: "Gallery Post Example",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img1, img2, img3],
            date: "Dec 19, 2013"
        },
        {
            id: 7,
            title: "Gallery Post",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img4],
            date: "Dec 21, 2013"
        },
        {
            id: 8,
            title: "Gallery Post Example",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img1, img2, img3],
            date: "Dec 19, 2013"
        },
        {
            id: 9,
            title: "Gallery Post",
            text:
                "A man who works with his hands is a laborer; a man who works with his hand s and his brain is a craftsman; but a man",
            image: [img4],
            date: "Dec 21, 2013"
        }
    ]
};

const blogReducer = (state = initstate, action) => {
    console.log("BlogReducer call", action);
    return state;
};

export default blogReducer;
