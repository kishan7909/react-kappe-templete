import img1 from "../../assert/upload/image1.jpg";
import img2 from "../../assert/upload/image2.jpg";
import img3 from "../../assert/upload/image3.jpg";
import img4 from "../../assert/upload/image4.jpg";
import img5 from "../../assert/upload/image5.jpg";
import img6 from "../../assert/upload/image6.jpg";
import img7 from "../../assert/upload/image7.jpg";
import img8 from "../../assert/upload/image8.jpg";
import img9 from "../../assert/upload/image9.jpg";
import img10 from "../../assert/upload/image10.jpg";
import img11 from "../../assert/upload/image11.jpg";
import img12 from "../../assert/upload/image12.jpg";
import thum1 from "../../assert/upload/thumb1.jpg";
import thum2 from "../../assert/upload/thumb2.jpg";
import thum3 from "../../assert/upload/thumb3.jpg";
import thum4 from "../../assert/upload/thumb4.jpg";
import thum5 from "../../assert/upload/thumb5.jpg";
import thum6 from "../../assert/upload/thumb6.jpg";
import thum7 from "../../assert/upload/thumb7.jpg";
import thum8 from "../../assert/upload/thumb8.jpg";
import thum9 from "../../assert/upload/thumb9.jpg";
import thum10 from "../../assert/upload/thumb10.jpg";
import thum11 from "../../assert/upload/thumb11.jpg";

const initstate = {
    project: [
        {
            id: "1",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img1,
            type: "Webdesign",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 130,
                    dates: "15 January, 2018",
                    website: "http:www.narola.in"
                },
                projectGallery: [thum1, thum2, thum3, thum4],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "2",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img2,
            type: "Webdesign",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum11, thum6, thum5],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code"
                ]
            }
        },
        {
            id: "3",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img3,
            type: "Photography",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum7, thum8],
                projectFeature: [
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "4",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img4,
            type: "Photography",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum11, thum2, thum3, thum4, thum9, thum10],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "5",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img5,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum5, thum9, thum7, thum4, thum6],
                projectFeature: [
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "6",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img6,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum1, thum2, thum3, thum4],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "7",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img7,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum10],
                projectFeature: ["Responsive Layout", "Font Awesome Icons"]
            }
        },
        {
            id: "8",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img8,
            type: "Nature",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum1, thum2, thum3, thum4],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "9",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img9,
            type: "Nature",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum10, thum2, thum9, thum7],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "10",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img10,
            type: "Logo",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum3, thum4, thum5],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "11",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img11,
            type: "Logo",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum1, thum2, thum3, thum4],
                projectFeature: ["Responsive Layout"]
            }
        },
        {
            id: "12",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img12,
            type: "Webdesign",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [thum11, thum5],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "13",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img1,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net",
                    projectFeature: [
                        "Responsive Layout",
                        "Font Awesome Icons",
                        "Clean & Commented Code",
                        "Pixel perfect Design",
                        "Highly Customizable"
                    ]
                },
                projectGallery: [
                    thum1,
                    thum2,
                    thum3,
                    thum4,
                    thum5,
                    thum6,
                    thum7,
                    thum8,
                    thum9,
                    thum10,
                    thum11
                ],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "14",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img4,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [
                    thum1,
                    thum2,
                    thum3,
                    thum4,
                    thum5,
                    thum6,
                    thum7,
                    thum8,
                    thum9,
                    thum10,
                    thum11
                ],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "15",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img5,
            type: "Logo",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [
                    thum1,
                    thum2,
                    thum3,
                    thum4,
                    thum5,
                    thum6,
                    thum7,
                    thum8,
                    thum9,
                    thum10,
                    thum11
                ],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        },
        {
            id: "16",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img2,
            type: "Nature",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                },
                projectGallery: [
                    thum1,
                    thum2,
                    thum3,
                    thum4,
                    thum5,
                    thum6,
                    thum7,
                    thum8,
                    thum9,
                    thum10,
                    thum11
                ],
                projectFeature: [
                    "Responsive Layout",
                    "Font Awesome Icons",
                    "Clean & Commented Code",
                    "Pixel perfect Design",
                    "Highly Customizable"
                ]
            }
        }
    ]
};

const projectReducer = (state = initstate, action) => {
    console.log("projectReducer call", action);
    return state;
};

export default projectReducer;
