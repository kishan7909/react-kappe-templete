/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";

import { BrowserRouter as Router, Route } from "react-router-dom";
import Project from "./components/projects/Project";
// import Sidebar from "./components/Navigation/Sidebar";
import ProjectDetails from "./components/projects/ProjectDetails";
import About from "./components/About/About";
import Blog from "./components/Blog/Blog";
import Contact from "./components/Contact/Contact";
import Infobox from "./components/SidebarInfoBox/Infobox";

class App extends Component {
    render() {
        return (
            <Router>
                <React.Fragment>
                    <div id="container">
                        {/* <Sidebar filter={true} /> */}
                        <Route exact path="/" component={Project} />
                        <Route path="/about" component={About} />
                        <Route path="/project/:id" component={ProjectDetails} />
                        <Route path="/blog" component={Blog} />
                        <Route path="/contact" component={Contact} />
                    </div>
                    <Infobox />
                </React.Fragment>
            </Router>
        );
    }
}

export default App;
